# 7-graus-test

[![Built with Lit](https://img.shields.io/badge/built%20with-Lit-blue.svg)](https://github.com/lit/lit)
[![pipeline status](https://gitlab.com/mario-aleo/7-graus-test/badges/dev/pipeline.svg)](https://gitlab.com/golfleet-tp/golfleet-app/-/commits/dev)
[![coverage report](https://gitlab.com/mario-aleo/7-graus-test/badges/dev/coverage.svg)](https://gitlab.com/golfleet-tp/golfleet-app/-/commits/dev)
[![Demo Storybook](https://img.shields.io/badge/Demo-Storybook-ff69b4)](https://golfleet-tp.gitlab.io/golfleet-app/)

## Architecture

Our goal is to create a frameworkless application by creating native [Web Components](https://developer.mozilla.org/en-US/docs/Web/Web_Components) using [Lit](https://lit.dev/), every component is designed using native Web APIs and an [off-main-thread](https://web.dev/off-main-thread/) architecture to ensure a fast, lightweight, interoperable, maintainable and future-proof application.

## Scripts

- `storybook` runs a live documentation of every component
- `start` runs app for development, reloading on file changes
- `start:build` runs app after it has been built using the build command
- `build` builds the app and outputs it in `dist` directory
- `test` runs test suite with @web/test-runner
- `lint` runs the linter for project
- `format` runs a code formatter using eslint and prettier rules

## Components

| Component                 | Documentation                                  | Links                                                                                      |
|---------------------------|------------------------------------------------|---------------------------------------------------------------------------------------------|
| `random-users`            | [README.md](components/random-users/README.md) | [Demo](https://golfleet-tp.gitlab.io/golfleet-app/?path=/story/random-users--random-users) |
