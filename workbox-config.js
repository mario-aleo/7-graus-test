const path = require('path');

module.exports = {
  swSrc: path.join(__dirname, 'serviceWorker.js'),
  swDest: path.join(__dirname, 'dist', 'sw.js'),
  globPatterns: [
    './assets/**/*.{ttf,svg,jpg,png}',
    './components/**/*.{js}',
    /** worker dependencies */
    './node_modules/comlink/dist/esm/**/*.{js}',
    /** */
  ],
  globDirectory: path.join(__dirname, 'dist'),
};
