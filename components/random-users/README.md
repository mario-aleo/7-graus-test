# random-users

An random user list component.

## Example

```html
<random-users pageSize="10"></random-users>
```

## Properties

| Property     | Attribute    | Type      | Default | Description                            |
|--------------|--------------|-----------|---------|----------------------------------------|
| `pageSize`   | `pageSize`   | `Number`  | 10      | Amount of users requested per request. |
| `_userList`  |              | `Array`   | []      | Current user list.                     |


## Methods

| Method            | Type                |
|-------------------|---------------------|
| `toggleTheme`     | `(): void`          |
| `requestNextPage` | `(): Promise<void>` |

## Events

| Event   | Type                        | Description                         |
|---------|-----------------------------|-------------------------------------|
| `sync-state`        | `CustomEvent<>` | Dispatched on _syncState ends.      |
| `user-list-changed` | `CustomEvent<>` | Dispatched on _userList update.     |
| `toggle-theme`      | `CustomEvent<>` | Dispatched on toggleTheme ends.     |
| `request-next-page` | `CustomEvent<>` | Dispatched on requestNextPage ends. |

