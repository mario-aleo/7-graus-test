import { fixture, expect, html } from '@open-wc/testing';

import '../random-users.js';

const awaitEvent = async (element, event, action) =>
  new Promise(resolve => {
    element.addEventListener(event, function __onEvent() {
      element.removeEventListener(event, __onEvent);
      resolve();
    });
    if (action) {
      action();
    }
  });

describe('<random-users>', () => {
  it('initialize properly', async () => {
    const element = await fixture(html`
      <random-users pageSize="10"></random-users>
    `);

    await awaitEvent(element, 'request-next-page');

    expect(element._userList).to.be.an('array').with.length(10);
  });

  it('request next page properly', async () => {
    const element = await fixture(html`
      <random-users pageSize="10"></random-users>
    `);

    await awaitEvent(element, 'request-next-page');

    expect(element._userList).to.be.an('array').with.length(10);

    await element.requestNextPage();

    expect(element._userList).to.be.an('array').with.length(20);

    await element.requestNextPage();

    expect(element._userList).to.be.an('array').with.length(30);
  });

  it('request next page properly when page-size changes', async () => {
    const element = await fixture(html`
      <random-users pageSize="20"></random-users>
    `);

    await awaitEvent(element, 'request-next-page');

    expect(element._userList).to.be.an('array').with.length(20);

    element.pageSize = 30;
    await element.requestNextPage();

    expect(element._userList).to.be.an('array').with.length(50);
  });

  it('request next page throws an error when page-size <= 0', async () => {
    const element = await fixture(html`
      <random-users pageSize="10"></random-users>
    `);

    let error = '';

    await awaitEvent(element, 'request-next-page');

    element.pageSize = 0;

    try {
      await element.requestNextPage();
    } catch (err) {
      error = err;
    }

    expect(error).to.be.an('Error');
    expect(error.message).to.equal('Page size must be greater than 0');
  });

  it('toggles theme properly', async () => {
    const element = await fixture(html`
      <random-users pageSize="10"></random-users>
    `);

    const expectedTheme = window.matchMedia('(prefers-color-scheme: dark)')
      .matches
      ? 'dark'
      : 'light';
    const counterTheme =
      localStorage.getItem('colorSchemePreference') === 'dark'
        ? 'light'
        : 'dark';

    expect(localStorage.getItem('colorSchemePreference')).to.be.equal(
      expectedTheme
    );

    element.toggleTheme();

    expect(localStorage.getItem('colorSchemePreference')).to.be.equal(
      counterTheme
    );
  });
});
