import { html } from 'lit';

import '../component.js';

export default {
  title: '<component>',
  component: 'component',
  argTypes: {
    attribute: {
      control: { type: 'boolean' },

      table: {
        category: 'Attributes',
        type: { summary: 'boolean' },
        defaultValue: { summary: 'false' },
      },

      description: '',
    },

    properties: {
      control: { type: 'boolean' },

      table: {
        category: 'Properties',
        type: { summary: 'boolean' },
        defaultValue: { summary: 'false' },
      },

      description: '',
    },

    event: {
      control: { type: null },

      table: {
        category: 'Events',
        type: { summary: null },
      },

      description: '',
    },

    slot: {
      control: { type: null },

      table: {
        category: 'Slots',
        type: { summary: null },
      },

      description: '',
    },

    // WORKAROUND: Should keep at least an empty slot to enable canvas's controls
    '': {
      control: false,

      table: {
        category: 'Slots',
        type: { summary: null },
      },
    },
  },
};

const Template = () => html`
  <style>
    component {
      width: 448px;
      max-width: 100%;
      box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
    }

    /**Ubuntu - 300 */
    @font-face {
      font-family: 'Ubuntu';
      font-style: normal;
      font-weight: 300;
      src: url('../../../assets/fonts/Ubuntu-Light.ttf') format('truetype');
    }

    /**Ubuntu - 400 */
    @font-face {
      font-family: 'Ubuntu';
      font-style: normal;
      font-weight: 400;
      src: url('../../../assets/fonts/Ubuntu-Regular.ttf') format('truetype');
    }

    /**Ubuntu - 500 */
    @font-face {
      font-family: 'Ubuntu';
      font-style: normal;
      font-weight: 500;
      src: url('../../../assets/fonts/Ubuntu-Medium.ttf') format('truetype');
    }
  </style>

  <component></component>
`;

export const Component = Template.bind({});

Component.args = {};
