import { html } from 'lit';

import '../random-users.js';

export default {
  title: '<random-users>',
  component: 'random-users',
  argTypes: {
    pageSize: {
      control: { type: 'number' },

      table: {
        category: 'Attributes',
        type: { summary: 'number' },
        defaultValue: { summary: '10' },
      },

      description: 'Amount of users requested per request.',
    },

    _userList: {
      control: null,

      table: {
        category: 'Properties',
        type: { summary: 'array' },
        defaultValue: { summary: '[]' },
      },

      description: 'Current user list.',
    },

    'sync-state': {
      control: { type: null },

      table: {
        category: 'Events',
        type: { summary: null },
      },

      description: 'Dispatched on _syncState ends',
    },
    'user-list-changed': {
      control: { type: null },

      table: {
        category: 'Events',
        type: { summary: null },
      },

      description: 'Dispatched on _userList update.',
    },
    'toggle-theme': {
      control: { type: null },

      table: {
        category: 'Events',
        type: { summary: null },
      },

      description: 'Dispatched on toggleTheme ends.',
    },
    'request-next-page': {
      control: { type: null },

      table: {
        category: 'Events',
        type: { summary: null },
      },

      description: 'Dispatched on requestNextPage ends.',
    },

    // WORKAROUND: Should keep at least an empty slot to enable canvas's controls
    '': {
      control: false,

      table: {
        category: 'Slots',
        type: { summary: null },
      },
    },
  },
};

const Template = ({ pageSize }) => html`
  <style>
    body {
      position: relative;
    }

    /** Saira - 300 */
    @font-face {
      font-family: 'Saira', sans-serif;
      font-style: normal;
      font-weight: 300;
      src: url('../../../assets/fonts/Saira-Light.ttf') format('truetype');
    }

    /** Saira - 400 */
    @font-face {
      font-family: 'Saira', sans-serif;
      font-style: normal;
      font-weight: 400;
      src: url('../../../assets/fonts/Saira-Regular.ttf') format('truetype');
    }

    /** Saira - 500 */
    @font-face {
      font-family: 'Saira', sans-serif;
      font-style: normal;
      font-weight: 500;
      src: url('../../../assets/fonts/Saira-Medium.ttf') format('truetype');
    }
  </style>

  <random-users pageSize="${pageSize}"></random-users>
`;

export const RandomUsers = Template.bind({});

RandomUsers.args = {
  pageSize: 10,
};
