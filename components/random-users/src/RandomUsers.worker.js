/* global importScripts, Comlink */

importScripts('../../../node_modules/comlink/dist/umd/comlink.js');

const service = {
  userList: [],

  addToUserList(params = {}) {
    const { userList = [] } = params;

    for (const user of userList) {
      this.userList.push({ name: user.name, picture: user.picture });
    }
  },
};

Comlink.expose(service);
