/* eslint-disable class-methods-use-this */
import { LitElement, html, css } from 'lit';
import * as Comlink from 'comlink';

/**
 * An random user list component.
 *
 * @element random-users
 *
 * @fires sync-state - Dispatched on _syncState ends.
 * @fires user-list-changed - Dispatched on _userList update.
 * @fires toggle-theme - Dispatched on toggleTheme ends.
 * @fires request-next-page - Dispatched on requestNextPage ends.
 *
 * @attr {Number} pageSize - Amount of users requested per request.
 *
 * @prop {type: Array} _userList - Current user list.
 *
 * @example
 * <random-users></random-users>
 *
 */
export class RandomUsers extends LitElement {
  static get properties() {
    return {
      /** Attribute */
      pageSize: {
        type: Number,
        reflect: true,
        attribute: true,
      },
      /** */

      /** Property */
      /** */

      /** State */
      _userList: { state: true },
      /** */
    };
  }

  static get styles() {
    return [
      /** host */
      css`
        :host {
          display: flex;
          flex-direction: column;
          grid-gap: 16px;
          justify-content: center;
          min-height: 100vh;
          max-width: 425px;
          padding: 16px 8px 32px;
          margin: auto;
          box-sizing: border-box;
        }

        :host * {
          outline: none;
        }

        button {
          padding: var(--theme-button-padding);
          border: var(--theme-button-border);
          border-radius: var(--theme-button-border-radius);
          margin: 0;
          font-size: var(--theme-button-font-size);
          font-family: var(--theme-button-font-family);
          letter-spacing: var(--theme-button-letter-spacing);
          text-align: var(--theme-button-text-align);
          color: var(--theme-button-color);
          background-color: var(--theme-button-background-color);
          outline: var(--theme-button-outline);
          cursor: var(--theme-button-cursor);
        }

        input {
          -webkit-appearance: var(--theme-input-webkit-appearance);
          padding: var(--theme-input-padding);
          border: var(--theme-input-border);
          border-radius: var(--theme-input-border-radius);
          margin: var(--theme-input-margin);
          font-family: var(--theme-input-font-family);
          letter-spacing: var(--theme-input-letter-spacing);
          font-size: var(--theme-input-font-size);
          color: var(--theme-input-color);
          background-color: var(--theme-input-background-color);
        }

        fieldset {
          padding: var(--theme-fieldset-padding);
          border: var(--theme-fieldset-border);
          border-radius: var(--theme-fieldset-border-radius);
          margin: var(--theme-fieldset-margin);
        }
      `,
      /** user */
      css`
        .user-card {
          display: flex;
          align-items: center;
          justify-content: flex-start;
          width: 100%;
          padding: 8px;
          background-color: var(--card-background);
          box-shadow: var(--low-elevation);
          box-sizing: border-box;
          contain: content;
        }

        .user-card img {
          width: 48px;
          height: 48px;
          margin-right: 16px;
          border-radius: 50%;
        }
      `,
      /** request */
      css`
        #request-next-page {
          position: absolute;
          left: 0;
          bottom: 8px;
          width: 50%;
          padding: 8px 16px;
          border-radius: 16px;
          text-align: center;
          color: var(--on-button-color, #fff);
          background-color: var(--button-color, #000);
          transform: translateX(50%);
          box-shadow: var(--high-elevation);
        }
      `,
      /** theme */
      css`
        #toggle-theme {
          position: fixed;
          bottom: 32px;
          right: 16px;
          display: flex;
          align-items: center;
          justify-content: center;
          width: 48px;
          height: 48px;
          border-radius: 50%;
          color: var(--on-button-color, #fff);
          background-color: var(--button-color, #000);
          box-shadow: var(--high-elevation);
        }

        #toggle-theme img {
          filter: var(--icon-filter);
        }
      `,
    ];
  }

  /** Lifecycle */
  constructor() {
    super();

    /** Attribute */
    this.pageSize = 10;
    /** */

    /** Property */
    /** */

    /** State */
    this._userList = [];
    /** */

    this.__api = 'https://randomuser.me/api/';

    this.__worker = new Worker(
      '/components/random-users/src/RandomUsers.worker.js'
    );
    this.__workerService = Comlink.wrap(this.__worker);
  }

  connectedCallback() {
    super.connectedCallback();

    if (localStorage.getItem('colorSchemePreference') === null) {
      if (window.matchMedia('(prefers-color-scheme: dark)').matches) {
        localStorage.setItem('colorSchemePreference', 'dark');
      } else {
        localStorage.setItem('colorSchemePreference', 'light');
      }

      document
        .querySelector('html')
        .setAttribute(
          'color-scheme',
          localStorage.getItem('colorSchemePreference')
        );
    }
  }

  firstUpdated() {
    this.requestNextPage();
  }
  /** */

  /** Render */
  render() {
    return html`
      ${this._userList.map(this._renderUser)}

      <button id="toggle-theme" @click="${() => this.toggleTheme()}">
        <img alt="theme" src="/assets/img/theme-icon.svg" />
      </button>

      <button id="request-next-page" @click="${() => this.requestNextPage()}">
        <b>Request next ${this.pageSize} users</b>
      </button>
    `;
  }

  _renderUser(user = {}) {
    return html`
      <section class="user-card">
        <img alt="User avatar" src="${user.picture.thumbnail}" />
        <span>${user.name.first} ${user.name.last}</span>
      </section>
    `;
  }
  /** */

  /** Private */
  async _syncState() {
    this._userList = await this.__workerService.userList;

    this.dispatchEvent(new CustomEvent('sync-state'));
    this.dispatchEvent(new CustomEvent('user-list-changed'));
  }
  /** */

  /** Public */
  toggleTheme() {
    if (localStorage.getItem('colorSchemePreference') === 'dark') {
      localStorage.setItem('colorSchemePreference', 'light');
    } else {
      localStorage.setItem('colorSchemePreference', 'dark');
    }

    document
      .querySelector('html')
      .setAttribute(
        'color-scheme',
        localStorage.getItem('colorSchemePreference')
      );

    this.dispatchEvent(new CustomEvent('toggle-theme'));
  }

  async requestNextPage() {
    if (this.pageSize <= 0) throw new Error('Page size must be greater than 0');

    const { results: userList } = await fetch(
      `${this.__api}/?results=${this.pageSize}`
    ).then(response => response.json());

    await this.__workerService.addToUserList({ userList });

    await this._syncState();

    this.dispatchEvent(new CustomEvent('request-next-page'));
  }
  /** */

  /** Observers */
  /** */
}
